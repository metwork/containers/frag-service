import subprocess
import shlex
from pathlib import Path
import os

import requests
from alembic.config import Config as AlembicConfig
from alembic.command import upgrade as alembic_upgrade, downgrade as alembic_downgrade

from app.api.endpoints import frag_files
from app.models.frag_file import FileStatusEnum, FileTypeEnum, IonChargeEnum

API_HOST = "http://127.0.0.1:8000"


def run_dev() -> None:
    cmd = "uvicorn app.main:app --reload"
    subprocess.run(shlex.split(cmd))


def init_dev() -> None:
    init_db()
    post_frag_files()


def init_db() -> None:
    alembic_config = AlembicConfig("alembic.ini")
    alembic_downgrade(alembic_config, "base")
    alembic_upgrade(alembic_config, "head")


def post_frag_files() -> None:

    data_path = Path(__file__).parent / "data"

    for file_type, file_path_ in (
        (FileTypeEnum.CONF, "param_config_{ion_charge}.txt"),
        (FileTypeEnum.PARAM, "param_output_{ion_charge}.log"),
    ):
        for ion_charge, label in (
            (IonChargeEnum.POSITIVE, "pos"),
            (IonChargeEnum.NEGATIVE, "neg"),
        ):
            file_path = data_path / file_path_.format(ion_charge=label)
            post_frag_file(
                file_type=file_type.value,
                ion_charge=ion_charge.value,
                file_path=file_path,
            )


def post_frag_file(file_type: str, ion_charge: str, file_path: Path) -> None:
    url = f"{API_HOST}{frag_files.ENDPOINT}"
    headers = {frag_files.X_API_KEY_LABEL: os.getenv(frag_files.API_KEY_ENV)}

    json = {
        "file_type": file_type,
        "ion_charge": ion_charge,
    }
    response = requests.post(url, json=json, headers=headers)
    if not response.status_code == 200:
        raise Exception(f"bad post response: {response}")
    data = response.json()
    file_id = data["id"]
    presigned_url = data["presigned_url"]

    with file_path.open("rb") as data:
        response = requests.put(presigned_url, data=data)

    url = f"{API_HOST}{frag_files.ENDPOINT}/{file_id}"
    file_status = FileStatusEnum.AVAILABLE.value
    data = {"file_status": file_status}
    response = requests.patch(url, json=data)
    print(response.json())
