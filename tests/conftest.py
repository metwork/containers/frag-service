import os
from typing import Generator
from urllib3.exceptions import MaxRetryError

from pydantic import BaseModel, AnyUrl
from sqlalchemy.orm import Session
from alembic.command import upgrade as alembic_upgrade, downgrade as alembic_downgrade
from alembic.config import Config as AlembicConfig
import pytest
from pytest_docker.plugin import Services
from fastapi.testclient import TestClient

from app import app
from app.db.session import SessionLocal
from app.storage.client import StorageClient


pytest_docker = os.getenv("PYTEST_DOCKER", "true").lower() == "true"


def init_db() -> None:
    alembic_config = AlembicConfig("alembic.ini")
    alembic_downgrade(alembic_config, "base")
    alembic_upgrade(alembic_config, "head")


@pytest.fixture
def client() -> TestClient:
    return TestClient(app)


def is_minio_responsive(minio_client: StorageClient) -> bool:
    try:
        minio_client.list_objects("/")
        return True
    except MaxRetryError:
        return False


@pytest.fixture(scope="session", autouse=pytest_docker)
def minio_service(
    storage_client: StorageClient, docker_services: Services
) -> StorageClient:
    """Ensure that HTTP service is up and responsive."""

    docker_services.wait_until_responsive(
        timeout=30.0, pause=0.1, check=lambda: is_minio_responsive(storage_client)
    )
    return storage_client


@pytest.fixture(scope="session")
def storage_client() -> Generator[StorageClient, None, None]:
    client = StorageClient()
    yield client


@pytest.fixture(scope="function")
def db() -> Generator[Session, None, None]:
    init_db()
    yield SessionLocal()


def is_db_responsive(db: Session) -> bool:
    try:
        db.execute("SELECT 1")  # type: ignore
        return True
    except Exception:
        return False


@pytest.fixture(scope="session", autouse=pytest_docker)
def db_service(docker_services: Services) -> Session:
    """Ensure that HTTP service is up and responsive."""

    db = SessionLocal()
    docker_services.wait_until_responsive(
        timeout=30.0, pause=0.1, check=lambda: is_db_responsive(db)
    )
    return db


class PresignedURLModel(BaseModel):
    url: AnyUrl
