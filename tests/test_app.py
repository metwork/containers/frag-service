from fastapi.testclient import TestClient
from app.api import HEALTH_CHECK_PATH


def test_health_check(client: TestClient) -> None:
    response = client.get(HEALTH_CHECK_PATH)
    assert response.status_code == 200
