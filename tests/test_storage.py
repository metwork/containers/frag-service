from uuid import uuid4

from pyminio import Pyminio

from .conftest import PresignedURLModel


def test_list_objects(storage_client: Pyminio) -> None:
    assert list(storage_client.list_objects()) == []


def test_presigned_put_object(storage_client: Pyminio) -> None:

    url = storage_client.presigned_put_object(str(uuid4()))
    assert PresignedURLModel(url=url)
