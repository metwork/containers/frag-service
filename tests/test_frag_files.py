from uuid import UUID, uuid4
from typing import Any, Dict, Callable, Optional, List
from pathlib import Path
import os
from enum import Enum

import pytest
from pytest import MonkeyPatch
import requests
from sqlalchemy.orm import Session
from fastapi.testclient import TestClient

from app import crud
from app.storage.client import StorageClient
from app.api.endpoints import frag_files
from app.models.frag_file import FileStatusEnum, FileTypeEnum, IonChargeEnum
from .conftest import PresignedURLModel


def validate_uuid4(uuid_string: str) -> bool:

    """
    Validate that a UUID string is in
    fact a valid uuid4.
    Happily, the uuid module does the actual
    checking for us.
    It is vital that the 'version' kwarg be passed
    to the UUID() call, otherwise any 32-character
    hex string is considered valid.
    """

    try:
        UUID(uuid_string, version=4)
        return True
    except ValueError:
        # If it's a value error, then the string
        # is not a valid hex code for a UUID.
        return False


def test_get_frag_files_empty_200(client: TestClient, db: Session) -> None:
    response = client.get(frag_files.ENDPOINT)
    assert response.status_code == 200


POST_FILE_TYPE = Callable[..., requests.Response]


@pytest.fixture
def _post_file(client: TestClient, db: Session) -> POST_FILE_TYPE:
    def func(
        json: Optional[Dict[str, str]] = {},
        headers: Optional[Dict[str, str]] = {},
    ) -> requests.Response:
        return client.post(frag_files.ENDPOINT, json=json, headers=headers)

    return func


@pytest.fixture
def create_data() -> Dict[str, str]:
    return {
        "file_type": FileTypeEnum.CONF.value,
        "ion_charge": IonChargeEnum.POSITIVE.value,
    }


@pytest.fixture
def apikey_header() -> Dict[str, Optional[str]]:
    return {frag_files.X_API_KEY_LABEL: os.getenv(frag_files.API_KEY_ENV)}


@pytest.fixture
def post_file(
    _post_file: POST_FILE_TYPE,
    apikey_header: Dict[str, str],
    create_data: Dict[str, str],
) -> POST_FILE_TYPE:
    def func() -> requests.Response:
        return _post_file(json=create_data, headers=apikey_header)

    return func


def test_post_frag_file_500(
    post_file: POST_FILE_TYPE,
    monkeypatch: MonkeyPatch,
) -> None:
    def raise_exception(*args: Any, **kwargs: Dict[Any, Any]) -> None:
        raise ValueError

    monkeypatch.setattr(crud.base.CRUDBase, "create", raise_exception)
    with pytest.raises(Exception):
        post_file()


def test_post_frag_file_422(
    _post_file: POST_FILE_TYPE, apikey_header: Dict[str, str]
) -> None:
    response = _post_file(headers=apikey_header)
    assert response.status_code == 422


def test_post_frag_file_403(
    _post_file: POST_FILE_TYPE, create_data: Dict[str, str]
) -> None:
    response = _post_file(json=create_data)
    assert response.status_code == 403


def test_post_frag_file_401(
    _post_file: POST_FILE_TYPE, create_data: Dict[str, str]
) -> None:
    headers = {frag_files.X_API_KEY_LABEL: "BAD_KEY"}
    response = _post_file(json=create_data, headers=headers)
    assert response.status_code == 401


def test_post_frag_file_200(
    client: TestClient,
    post_file: POST_FILE_TYPE,
    storage_client: StorageClient,
    shared_datadir: Path,
) -> None:
    response = post_file()
    assert response.status_code == 200
    data = response.json()
    file_id = data["id"]
    assert validate_uuid4(file_id)
    assert data["file_status"] == FileStatusEnum.NOT_UPLOADED.value

    presigned_url = data["presigned_url"]
    assert PresignedURLModel(url=presigned_url)
    assert not storage_client.object_exists(file_id)
    with open(shared_datadir / "dummy", "rb") as data:
        requests.put(presigned_url, data=data)
    assert storage_client.object_exists(file_id)

    response = client.get(f"{frag_files.ENDPOINT}/{file_id}")
    assert response.status_code == 200

    storage_client.remove_object(file_id)


@pytest.fixture
def posted_files(
    _post_file: POST_FILE_TYPE,
    apikey_header: Dict[str, str],
) -> List[str]:
    ids = []
    for file_type in (FileTypeEnum.CONF, FileTypeEnum.PARAM):
        for ion_charge in (IonChargeEnum.POSITIVE, IonChargeEnum.NEGATIVE):
            data = {
                "file_type": file_type.value,
                "ion_charge": ion_charge.value,
            }
            response = _post_file(json=data, headers=apikey_header)
            ids.append(response.json()["id"])
    return ids


def test_get_frag_files_with_data(client: TestClient, posted_files: List[str]) -> None:
    response = client.get(frag_files.ENDPOINT)
    assert response.status_code == 200
    data = response.json()
    assert len(data) == len(posted_files)
    assert set([item["id"] for item in data]) == set(posted_files)


@pytest.mark.parametrize(
    "key, enum_item",
    (
        ("ion_charge", IonChargeEnum.POSITIVE),
        ("file_type", FileTypeEnum.PARAM),
        ("file_status", FileStatusEnum.AVAILABLE),
    ),
)
def test_get_frag_files_with_filter(
    client: TestClient, posted_files: List[str], key: str, enum_item: Enum
) -> None:
    response = client.get(frag_files.ENDPOINT, params={key: enum_item.value})
    assert response.status_code == 200
    data = response.json()
    for item in data:
        assert item[key] == enum_item.value


def test_get_frag_file_with_id(client: TestClient, posted_files: List[str]) -> None:
    file_id = posted_files[0]
    url = f"{frag_files.ENDPOINT}/{file_id}"
    response = client.get(url)
    assert response.status_code == 200
    data = response.json()
    assert data["id"] == file_id


def test_patch_frag_file_status_200(
    client: TestClient, posted_files: List[str]
) -> None:
    file_id = posted_files[0]
    url = f"{frag_files.ENDPOINT}/{file_id}"
    file_status = FileStatusEnum.AVAILABLE.value
    data = {"file_status": file_status}
    response = client.patch(url, json=data)
    assert response.status_code == 200
    data = response.json()
    assert data["id"] == file_id
    assert data["file_status"] == file_status


def test_patch_frag_file_status_404(client: TestClient) -> None:
    file_id = uuid4()
    url = f"{frag_files.ENDPOINT}/{file_id}"
    file_status = FileStatusEnum.AVAILABLE.value
    data = {"file_status": file_status}
    response = client.patch(url, json=data)
    assert response.status_code == 404
