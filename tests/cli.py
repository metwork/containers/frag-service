import subprocess
import shlex


def run() -> None:
    print("### Run Flake8 ###")
    cmd = "flake8 app tests alembic"
    subprocess.run(shlex.split(cmd))

    print("### Run MyPy ###")
    cmd = "mypy --strict app tests"
    subprocess.run(shlex.split(cmd))

    print("### Run pytest ###")
    cmd = "pytest --cov-report term-missing --cov=app --disable-warnings"
    subprocess.run(shlex.split(cmd))
