from fastapi import APIRouter

from app.api.endpoints import health_check, frag_files, predict, spectrum


HEALTH_CHECK_PATH: str = "/health_check"


api_router = APIRouter()
api_router.include_router(
    health_check.router, prefix=HEALTH_CHECK_PATH, tags=["health"]
)
api_router.include_router(
    frag_files.router, prefix=frag_files.ENDPOINT, tags=["frag_file"]
)
api_router.include_router(predict.router, prefix=predict.ENDPOINT, tags=["predict"])
api_router.include_router(spectrum.router, prefix=spectrum.ENDPOINT, tags=["spectrum"])
