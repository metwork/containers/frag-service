from typing import Generator

from sqlalchemy.orm import Session

from app.storage.client import StorageClient
from app.db.session import SessionLocal


def get_db() -> Generator[Session, None, None]:
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()


def get_storage_client() -> Generator[StorageClient, None, None]:
    yield StorageClient()
