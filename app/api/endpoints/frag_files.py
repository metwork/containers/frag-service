from typing import List, Any, Optional
from uuid import UUID
import os

from sqlalchemy.orm import Session
from starlette import status
from fastapi import Depends, HTTPException
from fastapi.security import APIKeyHeader
from fastapi_utils.inferring_router import InferringRouter

from app import crud
from app.schemas.frag_file import (
    FragFileToCreate,
    FragFile,
    FragFileCreated,
    FragFileUpdate,
)
from app.storage.client import StorageClient
from app.api import deps
from app.models.frag_file import IonChargeEnum, FileTypeEnum, FileStatusEnum

ENDPOINT: str = "/frag_file"
X_API_KEY_LABEL = "X-API-Key"
X_API_KEY_HEADER = APIKeyHeader(name=X_API_KEY_LABEL)
API_KEY_ENV = "FRAG_FILE_API_KEY"

router = InferringRouter()


def check_api_key_header(x_api_key: str = Depends(X_API_KEY_HEADER)) -> bool:
    """takes the X-API-Key header and check it"""

    if not x_api_key == os.getenv(API_KEY_ENV):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid API Key",
        )
    return True


@router.get("", response_model=List[FragFile])
def read_frag_files(
    ion_charge: Optional[IonChargeEnum] = None,
    file_type: Optional[FileTypeEnum] = None,
    file_status: Optional[FileStatusEnum] = None,
    db: Session = Depends(deps.get_db),
    skip: int = 0,
    limit: int = 100,
) -> Any:
    items = crud.frag_file.get_multi(
        db,
        skip=skip,
        limit=limit,
        ion_charge=ion_charge,
        file_type=file_type,
        file_status=file_status,
    )
    return items


@router.get("/{file_id}", response_model=FragFile)
def read_frag_file(
    file_id: UUID,
    db: Session = Depends(deps.get_db),
) -> Any:
    item = crud.frag_file.get(db=db, id=file_id)
    return item


@router.post("", response_model=FragFileCreated)
def create_frag_file(
    obj_in: FragFileToCreate,
    db: Session = Depends(deps.get_db),
    storage_client: StorageClient = Depends(deps.get_storage_client),
    check_auth: bool = Depends(check_api_key_header),
) -> Any:
    """
    Create new fragmentation file.
    """
    item = crud.frag_file.create_with_presigned_url(
        db=db, obj_in=obj_in, storage_client=storage_client
    )
    return item


@router.patch("/{file_id}", response_model=FragFile)
def modify_frag_file(
    file_id: UUID,
    obj_in: FragFileUpdate,
    db: Session = Depends(deps.get_db),
) -> Any:
    frag_file = crud.frag_file.get(db=db, id=file_id)
    if not frag_file:
        raise HTTPException(status_code=404, detail="File not found")
    item = crud.frag_file.update(db=db, db_obj=frag_file, obj_in=obj_in)
    return item
