from typing import Any, List, Optional

from sqlalchemy.orm import Session
from fastapi import Depends
from fastapi_utils.inferring_router import InferringRouter

from app import crud
from app.api import deps
from app.schemas.spectrum import Spectrum, SpectrumCreate

ENDPOINT: str = "/spectrum"
router = InferringRouter()


@router.get("", response_model=List[Spectrum])
def read_spectrum(
    smiles: Optional[str] = None,
    cfm_predict_hash: Optional[str] = None,
    db: Session = Depends(deps.get_db),
    skip: int = 0,
    limit: int = 100,
) -> Any:
    items = crud.spectrum.get_multi(
        db, skip=skip, limit=limit, smiles=smiles, cfm_predict_hash=cfm_predict_hash
    )
    return items


@router.post("", response_model=Spectrum)
def create_spectrum(
    obj_in: SpectrumCreate,
    db: Session = Depends(deps.get_db),
) -> Any:
    item = crud.spectrum.create(db=db, obj_in=obj_in)
    return item
