from typing import Any

from sqlalchemy.orm import Session
from fastapi import Depends
from fastapi_utils.inferring_router import InferringRouter
from celery import Celery


from app import crud
from app.core.config import settings
from app.api import deps
from app.schemas.predict import PredictIn


ENDPOINT: str = "/predict"
CFM_PREDICT_HASH = "cfm_predict_hash"
router = InferringRouter()

app = Celery("hello", broker="amqp://cfmid:BROKER_PASSWORD@cfmid_broker/cfmid")


@router.post("")  # , response_model=List[Spectrum])
def predict(
    params: PredictIn,
    db: Session = Depends(deps.get_db),
) -> Any:
    result = crud.spectrum.get_multi(
        db, smiles=params.smiles, cfm_predict_hash=cfm_predict_hash
    )
    if len(result) == 0:

        app.send_task(
            "predict",
            kwargs={
                "smiles": params.smiles,
                "params": {},
                "callback": {"url": f"{settings.SERVER_HOST}/spectrum"},
            },
        )
    # return result
