from pydantic import BaseModel
from fastapi_utils.inferring_router import InferringRouter

router = InferringRouter()


class HealthModel(BaseModel):
    status: str


@router.get("")
async def health_check() -> HealthModel:
    return HealthModel(status="ok")
