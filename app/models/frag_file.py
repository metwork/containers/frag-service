import enum
from uuid import uuid4
from sqlalchemy import Column, Enum, String
from sqlalchemy.dialects.postgresql import UUID, JSONB
from app.db.base_class import Base


class FileStatusEnum(str, enum.Enum):
    NOT_UPLOADED = "NOT_UPLOADED"
    AVAILABLE = "AVAILABLE"
    ERROR = "ERROR"


class FileTypeEnum(str, enum.Enum):
    PARAM = "PARAM"
    CONF = "CONF"


class IonChargeEnum(str, enum.Enum):
    POSITIVE = "POSITIVE"
    NEGATIVE = "NEGATIVE"


class FragFile(Base):
    id = Column(UUID(as_uuid=True), default=uuid4, primary_key=True, index=True)
    file_status = Column(Enum(FileStatusEnum), index=True, nullable=False)
    file_type = Column(Enum(FileTypeEnum), index=True, nullable=False)
    ion_charge = Column(Enum(IonChargeEnum), index=True, nullable=False)
    file_metadata = Column(JSONB)
    file_hash = Column(String)
