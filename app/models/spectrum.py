from uuid import uuid4
from sqlalchemy import Column, String, Float
from sqlalchemy.dialects.postgresql import UUID, JSONB, ARRAY
from app.db.base_class import Base


class Spectrum(Base):
    id = Column(UUID(as_uuid=True), default=uuid4, primary_key=True, index=True)
    smiles = Column(String, index=True, nullable=False)
    cfm_predict_hash = Column(String, index=True, nullable=False)
    spectrum_metadata = Column(JSONB)
    peaks_json = Column(ARRAY(Float, dimensions=2))
