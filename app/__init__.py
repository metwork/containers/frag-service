import pkg_resources
from .main import app

__version__ = pkg_resources.get_distribution("app").version
__all__ = ["app"]
