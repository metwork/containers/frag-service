from typing import Any

from starlette.requests import Request
from starlette.responses import JSONResponse
from fastapi import FastAPI

from app.api import api_router


app = FastAPI(title="CFM-ID Service")


@app.exception_handler(Exception)
async def default_exception_handler(request: Request, exc: Any) -> JSONResponse:
    return JSONResponse({"detail": "Internal error"}, status_code=500)


app.include_router(api_router)
