from typing import Optional, Any, Dict

from minio import Minio

from app.core.config import settings


class StorageClient:
    def __init__(self, bucket_name: Optional[str] = None):
        self._bucket_name = bucket_name or settings.MINIO_BUCKET
        self._client = Minio(
            endpoint=f"{settings.MINIO_HOST}:{settings.MINIO_PORT}",
            access_key="minioadmin",
            secret_key="minioadmin",
            secure=False,
        )
        self.create_bucket_if_not_exists()

    def create_bucket_if_not_exists(self) -> None:
        if not self._client.bucket_exists(self._bucket_name):
            self._client.make_bucket(self._bucket_name)

    def list_objects(self, *args: Any, **kwargs: Dict[Any, Any]) -> Any:
        return self._client.list_objects(self._bucket_name, *args, **kwargs)

    def presigned_put_object(self, *args: Any, **kwargs: Dict[Any, Any]) -> Any:
        return self._client.presigned_put_object(self._bucket_name, *args, **kwargs)

    def object_exists(self, object_name: str) -> bool:
        try:
            self._client.stat_object(self._bucket_name, object_name)
            return True
        except Exception:
            return False

    def remove_object(
        self, object_name: str, *args: Any, **kwargs: Dict[Any, Any]
    ) -> None:
        self._client.remove_object(self._bucket_name, object_name)
