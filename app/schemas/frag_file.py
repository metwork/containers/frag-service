# from typing import Optional
from uuid import UUID
from typing import Optional

from pydantic import BaseModel, AnyUrl

from app.models.frag_file import FileStatusEnum, FileTypeEnum, IonChargeEnum


# Shared properties
class FragFileBase(BaseModel):
    file_type: FileTypeEnum
    ion_charge: IonChargeEnum
    # title: Optional[str] = None
    # description: Optional[str] = None


# Properties to receive on FragFile creation
class FragFileToCreate(FragFileBase):
    pass
    # title: str


# Properties to receive on FragFile update
class FragFileUpdate(BaseModel):
    file_status: FileStatusEnum


# Properties shared by models stored in DB
class FragFileInDBBase(FragFileBase):

    file_hash: Optional[str]
    # owner_id: int

    class Config:
        orm_mode = True


# Properties to return to client
class FragFile(FragFileInDBBase):
    id: UUID
    file_status: FileStatusEnum


# Properties properties stored in DB
class FragFileInDB(FragFileInDBBase):
    pass


class FragFileCreated(FragFile):
    presigned_url: AnyUrl
    # title: str
