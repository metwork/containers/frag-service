# from typing import Optional
from uuid import UUID

from pydantic import BaseModel, conlist, Field


class SpectrumMetadata(BaseModel):
    source: str = "cfm-id"
    smiles: str = Field(example="CCOCCC")
    cfm_predict_hash: str = Field(example="8ab73d7725c59ebcd238346da6e56102")
    energy: str


# Shared properties
class SpectrumBase(BaseModel):
    smiles: str
    cfm_predict_hash: str = Field(example="8ab73d7725c59ebcd238346da6e56102")
    spectrum_metadata: SpectrumMetadata
    peaks_json: conlist(conlist(float, min_items=2, max_items=2))  # type: ignore


# Properties to receive on Spectrum creation
class SpectrumCreate(SpectrumBase):
    pass
    # title: str


# Properties to receive on Spectrum update
class SpectrumUpdate(BaseModel):
    pass


# Properties shared by models stored in DB
class SpectrumInDBBase(SpectrumBase):

    # title: str
    # owner_id: int

    class Config:
        orm_mode = True


# Properties to return to client
class Spectrum(SpectrumInDBBase):
    id: UUID


# Properties properties stored in DB
class SpectrumInDB(SpectrumInDBBase):
    pass
