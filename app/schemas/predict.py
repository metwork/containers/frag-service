from uuid import UUID

from pydantic import BaseModel, Field, conlist

from app.schemas.spectrum import SpectrumMetadata


class PredictIn(BaseModel):
    smiles: str = Field(example="CCOCCC")
    file_param: UUID
    file_conf: UUID


class PredictOut(SpectrumMetadata):
    peaks_json: conlist(conlist(float, min_items=2, max_items=2))  # type: ignore
