from typing import Any

from sqlalchemy.orm import Session

from app.storage.client import StorageClient
from app.crud.base import CRUDBase
from app.models.frag_file import FragFile, FileStatusEnum
from app.schemas.frag_file import FragFileToCreate, FragFileUpdate


class CRUDFragFile(CRUDBase[FragFile, FragFileToCreate, FragFileUpdate]):
    def get_obj_in_data_create(self, obj_in: FragFileToCreate) -> Any:
        obj_in_data = super().get_obj_in_data_create(obj_in)
        return {**obj_in_data, "file_status": FileStatusEnum.NOT_UPLOADED}

    def create_with_presigned_url(
        self, db: Session, obj_in: FragFileToCreate, storage_client: StorageClient
    ) -> FragFile:
        db_obj = self.create(db=db, obj_in=obj_in)
        url = storage_client.presigned_put_object(str(db_obj.id))
        setattr(db_obj, "presigned_url", url)
        return db_obj


frag_file = CRUDFragFile(FragFile)
