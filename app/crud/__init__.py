from .crud_frag_file import frag_file
from .crud_spectrum import spectrum

__all__ = ["frag_file", "spectrum"]

# For a new basic set of CRUD operations you could just do

# from .base import CRUDBase
# from app.models.item import Item
# from app.schemas.item import ItemCreate, ItemUpdate

# item = CRUDBase[Item, ItemCreate, ItemUpdate](Item)
