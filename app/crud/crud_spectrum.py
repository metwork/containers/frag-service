from app.crud.base import CRUDBase
from app.models.spectrum import Spectrum
from app.schemas.spectrum import SpectrumCreate, SpectrumUpdate

spectrum = CRUDBase[Spectrum, SpectrumCreate, SpectrumUpdate](Spectrum)
